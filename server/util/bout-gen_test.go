package util

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/bowerscd/bc/server/database"
)

var errShouldBeEqual = errors.New("Values should be equal")

func TestCombinations(t *testing.T) {
	rv := generateCombinations(5, 2)

	expected := [][]uint8{
		{0, 1},
		{0, 2},
		{0, 3},
		{0, 4},
		{1, 2},
		{1, 3},
		{1, 4},
		{2, 3},
		{2, 4},
		{3, 4},
	}

	if !reflect.DeepEqual(expected, rv) {
		t.Errorf("\nExpected:\t%v\nRecieved:\t%v", expected, rv)
	}
}

func TestBoutOrder(t *testing.T) {
	rv := GenerateOrder(5)

	expected := [][]uint8{
		{0, 1}, {2, 3}, {0, 4}, {1, 2}, {3, 4}, {0, 2}, {1, 3}, {2, 4}, {0, 3}, {1, 4},
	}

	if !reflect.DeepEqual(expected, rv) {
		t.Errorf("\nExpected:\t%v\nRecieved:\t%v", expected, rv)
	}

	hiBee := database.Fencer{
		Name:    "Hi",
		Surname: "Bee",
	}

	rosaDiaz := database.Fencer{
		Name:    "Rosa",
		Surname: "Diaz",
	}

	dougJudy := database.Fencer{
		Name:    "Doug",
		Surname: "Judy",
	}

	raymondHolt := database.Fencer{
		Name:    "Raymond",
		Surname: "Holt",
	}

	terryCrews := database.Fencer{
		Name:    "Terry",
		Surname: "Crews",
	}

	f := []database.Fencer{
		hiBee,
		rosaDiaz,
		dougJudy,
		raymondHolt,
		terryCrews,
	}

	order := convertToOrder(f, rv, false)
	if len(order) != 10 {
		t.Fail()
	}

	expected2 := [][]database.Fencer{
		[]database.Fencer{hiBee, rosaDiaz},
		[]database.Fencer{dougJudy, raymondHolt},
		[]database.Fencer{hiBee, terryCrews},
		[]database.Fencer{rosaDiaz, dougJudy},
		[]database.Fencer{raymondHolt, terryCrews},
		[]database.Fencer{hiBee, dougJudy},
		[]database.Fencer{rosaDiaz, raymondHolt},
		[]database.Fencer{dougJudy, terryCrews},
		[]database.Fencer{hiBee, raymondHolt},
		[]database.Fencer{rosaDiaz, terryCrews},
	}

	for i := range expected2 {
		if !reflect.DeepEqual(expected2[i], order[i]) {
			t.Errorf(`
			[INDEX: %d] Values Should Be Equal:
				EXPECTED: (%s %s, %s %s) vs GOT: (%s %s, %s %s)`,
				i,
				expected2[i][0].Name, expected2[i][0].Surname,
				expected2[i][1].Name, expected2[i][1].Surname,
				order[i][0].Name, order[i][0].Surname,
				order[i][1].Name, order[i][1].Surname)
		}
	}

	// if !reflect.DeepEqual(expected2, order) {
	// 	t.Errorf("Values Should Be Equal:\n\t%v\n\t%v", expected2, order)
	// }
}

func TestHardCodedOrder(t *testing.T) {

	hiBee := database.Fencer{
		Name:    "Hi",
		Surname: "Bee",
	}

	rosaDiaz := database.Fencer{
		Name:    "Rosa",
		Surname: "Diaz",
	}

	dougJudy := database.Fencer{
		Name:    "Doug",
		Surname: "Judy",
	}

	raymondHolt := database.Fencer{
		Name:    "Raymond",
		Surname: "Holt",
	}

	terryCrews := database.Fencer{
		Name:    "Terry",
		Surname: "Crews",
	}

	f := []database.Fencer{
		hiBee,
		rosaDiaz,
		dougJudy,
		raymondHolt,
		terryCrews,
	}

	resultList, err := RetrievePreSelectedOrder(f, 0)
	if err != nil {
		t.Error(err)
	}

	expected := [][]database.Fencer{
		[]database.Fencer{hiBee, rosaDiaz},
		[]database.Fencer{dougJudy, raymondHolt},
		[]database.Fencer{terryCrews, hiBee},
		[]database.Fencer{rosaDiaz, dougJudy},
		[]database.Fencer{terryCrews, raymondHolt},
		[]database.Fencer{hiBee, dougJudy},
		[]database.Fencer{rosaDiaz, terryCrews},
		[]database.Fencer{raymondHolt, hiBee},
		[]database.Fencer{dougJudy, terryCrews},
		[]database.Fencer{raymondHolt, rosaDiaz},
	}

	if !reflect.DeepEqual(resultList, expected) {
		t.Error(errShouldBeEqual)
	}
}
