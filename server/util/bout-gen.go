package util

import (
	"gitlab.com/bowerscd/bc/server/database"
)

func generateCombinations(elements int, choose int) [][]uint8 {
	rv := [][]uint8{}

	a := make([]int, choose)

	for i := 0; i < choose; i++ {
		a[i] = i
	}

	i := choose - 1

	for a[0] < elements-choose+1 {
		for i > 0 && a[i] == elements-choose+i {
			i--
		}
		tmp := make([]uint8, choose)
		for i := range a {
			tmp[i] = uint8(a[i])
		}

		rv = append(rv, tmp)

		a[i]++

		for i < choose-1 {
			a[i+1] = a[i] + 1
			i++
		}
	}

	return rv
}

func generateOrderWorker(matches, result [][]uint8) [][]uint8 {
	rv := [][]uint8{}
	if len(matches) == 0 {
		return result
	}

	for i, match := range matches {
		matchCopy := deepcopy(matches)

		if len(result) == 0 {
			newResult := [][]uint8{match}
			newMatches := append(matchCopy[:i], matchCopy[i+1:]...)
			rv = generateOrderWorker(newMatches, newResult)
		} else {
			previousMatch := result[len(result)-1]
			if !in(previousMatch[0], match) && !in(previousMatch[1], match) {
				newResult := deepcopy(result)

				newResult = append(newResult, match)
				newMatches := append(matchCopy[:i], matchCopy[i+1:]...)
				rv = generateOrderWorker(newMatches, newResult)

			}
		}

		if len(rv) > 0 {
			return rv
		}
	}

	return [][]uint8{}
}

func deepcopy(src [][]uint8) [][]uint8 {
	rv := make([][]uint8, len(src))

	for i := 0; i < len(src); i++ {
		rv[i] = make([]uint8, len(src[i]))

		copy(rv[i], src[i])
	}

	return rv
}

func in(element uint8, tuple []uint8) bool {
	return element == tuple[0] || element == tuple[1]
}

// GenerateOrder - generate a dynamic bouting order for the number of fencers given.
// 		the only requirement is that a person is not required to bout twice in a row.
func GenerateOrder(fencers uint8) [][]uint8 {
	totalMatches := generateCombinations(int(fencers), 2)
	rv := generateOrderWorker(totalMatches, [][]uint8{})

	return rv
}

func convertToOrder(elements []database.Fencer, order [][]uint8, fromOfficial bool) [][]database.Fencer {

	rv := make([][]database.Fencer, len(order))

	for i, pair := range order {
		first := int(pair[0])
		second := int(pair[1])

		if fromOfficial {
			first--
			second--
		}

		rv[i] = make([]database.Fencer, len(pair))
		rv[i][0] = elements[first]
		rv[i][1] = elements[second]
	}

	return rv
}
