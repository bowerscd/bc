package database

import (
	"errors"
	"log"
	"os"
	"testing"
)

const DBURI = "testdb.sqlite"
const DBTYPE = SQLITE

var errShouldError = errors.New("should have errored out, but did not")
var errAssertionFail = errors.New("assertion failure")

var johnJacobson = Fencer{
	Name:    "John",
	Surname: "Jacobson",
}

var helloWorld = Fencer{
	Name:    "Hello",
	Surname: "World",
}

var janeJohnson = Fencer{
	Name:    "Jane",
	Surname: "Johnson",
}

var weiWang = Fencer{
	Name:    "伟",
	Surname: "王",
}

var yumiSato = Fencer{
	Name:    "夕実",
	Surname: "佐藤",
}

var bostonFC = Club{
	Name:         "Boston Fencing Club",
	Abbreviation: "BFC",
}

var progFC = Club{
	Name:         "Programming Club Unlimited",
	Abbreviation: "PCU",
}

var sapporoClub = Club{
	Name:         "札幌フェンシング協会練習会",
	Abbreviation: "SC",
}

func Setup(t *testing.T) {
	log.SetFlags(log.Llongfile)

	if _, err := os.Stat(DBURI); err == nil {
		os.Remove(DBURI)
	}

	err := ConnectToDB(DBTYPE, DBURI)
	if err != nil {
		t.Error()
	}
}

func Teardown(t *testing.T) {
	err := CloseDB()
	if err != nil {
		t.Error()
	}
	if _, err := os.Stat(DBURI); err == nil {
		//os.Remove(DBURI)
	}
}

func TestInsertFencer(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	args := []Fencer{
		johnJacobson,
		helloWorld,
		janeJohnson,
		weiWang,
		yumiSato,
	}

	for _, v := range args {
		err := InsertFencer(v)
		if err != nil {
			t.Error(err)
		}
	}
}

func TestSelectFencerFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	_, err := SelectFencer(weiWang)
	if err == nil {
		t.Error(errShouldError)
	}

	f, err := SelectAllFencer(weiWang)
	if err != nil {
		t.Error(err)
	}

	if len(f) != 0 {
		t.Error(errAssertionFail)
	}

}

func TestUpdateFencer(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	f, err := SelectFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	fNew := f

	fNew.Name = "Jacob"
	fNew.Surname = "Johnson"

	err = UpdateFencer(fNew, f)
	if err != nil {
		t.Error(err)
	}

	err = UpdateFencer(weiWang, weiWang)
	if err == nil {
		t.Error(errShouldError)
	}
}

func TestDeleteFencer(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	err = DeleteFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	_, err = SelectFencer(johnJacobson)
	if err == nil {
		t.Error(errShouldError)
	}

}

func TestDeleteFencerFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := DeleteFencer(johnJacobson)
	if err == nil {
		t.Error(errShouldError)
	}

}

func TestInsertFencerFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	f, err := SelectFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	err = InsertFencer(f)
	if err == nil {
		t.Error(errShouldError)
	}
}

func TestMultiInsertFencer(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	args := []Fencer{
		johnJacobson,
		johnJacobson,
		johnJacobson,
		johnJacobson,
		johnJacobson,
	}

	for _, v := range args {
		err := InsertFencer(v)
		if err != nil {
			t.Error(err)
		}
	}

	fe, err := SelectAllFencer(args[0])
	if err != nil {
		t.Error(err)
	}

	if len(fe) != len(args) {
		t.Error("failed to return same number of records as input")
	}
}

func TestInsertClub(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	args := []Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	for _, v := range args {
		err := InsertClub(v)
		if err != nil {
			t.Error(err)
		}
	}
}

func TestSelectClubFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	_, err := SelectClub(bostonFC)
	if err == nil {
		t.Error(errShouldError)
	}

	f, err := SelectAllClub(bostonFC)
	if err != nil {
		t.Error(err)
	}

	if len(f) != 0 {
		t.Error(errAssertionFail)
	}

}

func TestUpdateClub(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertClub(bostonFC)
	if err != nil {
		t.Error(err)
	}

	f, err := SelectClub(bostonFC)
	if err != nil {
		t.Error(err)
	}

	fNew := f

	fNew.Name = "Baltimore FC"
	fNew.Abbreviation = "BAFC"

	err = UpdateClub(fNew, f)
	if err != nil {
		t.Error(err)
	}

	err = UpdateClub(progFC, progFC)
	if err == nil {
		t.Error(errShouldError)
	}
}

func TestDeleteClub(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertClub(sapporoClub)
	if err != nil {
		t.Error(err)
	}

	err = DeleteClub(sapporoClub)
	if err != nil {
		t.Error(err)
	}

	_, err = SelectClub(sapporoClub)
	if err == nil {
		t.Error(errShouldError)
	}

}

func TestDeleteClubFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := DeleteClub(sapporoClub)
	if err == nil {
		t.Error(errShouldError)
	}

}

func TestInsertClubFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertClub(progFC)
	if err != nil {
		t.Error(err)
	}

	f, err := SelectClub(progFC)
	if err != nil {
		t.Error(err)
	}

	err = InsertClub(f)
	if err == nil {
		t.Error(errShouldError)
	}
}

func TestMultiInsertClubFail(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	args := []Club{
		bostonFC,
		bostonFC,
		bostonFC,
		bostonFC,
		bostonFC,
	}

	err := InsertClub(bostonFC)
	if err != nil {
		t.Error(err)
	}

	for _, v := range args {
		err = InsertClub(v)
		if err == nil {
			t.Error(errShouldError)
		}
	}

	fe, err := SelectAllClub(args[0])
	if err != nil {
		t.Error(err)
	}

	if len(fe) != 1 {
		t.Error("failed to return same number of records as input")
	}
}

func TestInsertRelation(t *testing.T) {
	Setup(t)
	defer Teardown(t)

	err := InsertFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	err = InsertClub(bostonFC)
	if err != nil {
		t.Error(err)
	}

	c, err := SelectClub(bostonFC)
	if err != nil {
		t.Error(err)
	}

	f, err := SelectFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	newF := f

	newF.PrimaryClub = c

	err = UpdateFencer(newF, f)
	if err != nil {
		t.Error(err)
	}

	f, err = SelectFencer(johnJacobson)
	if err != nil {
		t.Error(err)
	}

	if f.PrimaryClub.Abbreviation == c.Abbreviation {
		t.Error(errAssertionFail)
	}

}
