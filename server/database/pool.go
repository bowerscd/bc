package database

import (
	"encoding/json"
	"errors"
	"log"
)

//ErrPoolComplete - the pool is marked as finished, thus is read only. mark uncomplete first
var ErrPoolComplete = errors.New("pool is already complete; need to unfinish first")

//ErrValidationFailed - a validation error occured when attempting to update pool scores
var ErrValidationFailed = errors.New("pool scores validation failed")

func parsePool(pool *Pool) error {
	return json.Unmarshal([]byte(pool.RawData), &pool.PoolInternal)
}

func stringifyPool(pool *Pool) (string, error) {
	b, err := json.Marshal(pool.PoolInternal)

	return string(b), err
}

func buildInternal(scores [][]uint, pool *Pool) error {
	pi := PoolInternal{
		Scores:             make([][]uint32, len(scores)),
		VictoryPercentages: make([]float32, len(scores)),
		TouchesScored:      make([]uint32, len(scores)),
		TouchesRecieved:    make([]uint32, len(scores)),
		Indicator:          make([]int8, len(scores)),
		Placement:          make([]uint32, len(scores)),
	}

	for i := 0; i < len(scores); i++ {
		pi.Scores[i] = make([]uint32, len(scores[i]))

		TS := uint32(0)
		V := float32(0)
		TR := uint32(0)

		for j := 0; j < len(scores[i]); j++ {
			// Iterate Rows
			TS += uint32(scores[i][j])

			pi.Scores[i][j] = uint32(scores[i][j])

			if (i != j && scores[i][j] == scores[j][i]) ||
				scores[i][i] != 0 {
				log.Print(ErrValidationFailed)
				return ErrValidationFailed
			}

			if scores[i][j] > scores[j][i] {
				V++
			}

			// Iterate Column
			TR += uint32(scores[j][i])
		}

		I := int8(TS) - int8(TR)

		pi.Indicator[i] = I
		pi.VictoryPercentages[i] = V / float32(len(scores)-1)
		pi.TouchesRecieved[i] = TR
		pi.TouchesScored[i] = TS
	}

	return nil
}

func stringifyPoolInternal(pool *PoolInternal) (string, error) {
	b, err := json.Marshal(pool)

	return string(b), err
}

// SetPoolPublishState - set the IsCompleted field for a pool
func SetPoolPublishState(id uint, state bool) error {
	p := Pool{}
	p.ID = id

	p, err := SelectPool(p)
	if err != nil {
		log.Println(err)
		return err
	}

	p.IsComplete = state
	p.Fencers = nil

	err = dbConn.Save(&p).Error
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

// UpdatePool - update a pool from an internal object
func UpdatePool(p Pool, scores [][]uint) error {

	pool, err := SelectPool(p)
	if err != nil {
		log.Print(err)
		return err
	}

	if pool.IsComplete {
		log.Println(ErrPoolComplete)
		return ErrPoolComplete
	}

	err = buildInternal(scores, &p)
	if err != nil {
		return err
	}

	str, err := stringifyPoolInternal(&p.PoolInternal)
	if err != nil {
		log.Print(err)
		return err
	}

	pool.RawData = str

	return nil
}

// GeneratePool - generate an empty pool from a list of fencers and place it into the database
func GeneratePool(fList []Fencer) error {
	p := Pool{}
	p.Fencers = fList

	p.PoolInternal.Placement = make([]uint32, len(fList))
	p.PoolInternal.TouchesRecieved = make([]uint32, len(fList))
	p.PoolInternal.TouchesScored = make([]uint32, len(fList))
	p.PoolInternal.VictoryPercentages = make([]float32, len(fList))
	p.PoolInternal.Indicator = make([]int8, len(fList))
	p.PoolInternal.Scores = make([][]uint32, len(fList))

	for i := range fList {
		p.PoolInternal.Scores[i] = make([]uint32, len(fList))
	}

	data, err := stringifyPool(&p)
	if err != nil {
		log.Println(err)
		return err
	}

	p.RawData = data

	err = dbConn.Create(&p).Error
	if err != nil {
		log.Println(err)
	}
	return err

}

// SelectPool - select a pool from the database
// returns the first found record. There may be duplicates.
func SelectPool(p Pool) (Pool, error) {
	dbObj := Pool{}

	p.Fencers = nil

	err := dbConn.Where(&p).Preload("Fencers").First(&dbObj).Error
	if err != nil {
		log.Println(err)
		return Pool{}, err
	}

	err = parsePool(&dbObj)
	if err != nil {
		log.Println(err)
		return Pool{}, err
	}

	return dbObj, err
}

// DeletePool - delete a pool from the database
// throws an error if the delete did nothing
func DeletePool(p Pool) error {
	p.Fencers = nil

	db := dbConn.Where(&p).Delete(&p)

	err := db.Error
	affected := db.RowsAffected
	if err != nil {
		log.Println(err)
		return err
	}

	if affected == 0 {
		log.Println(errNoSuchRecord)
		return errNoSuchRecord
	}

	return nil
}

// SelectAllPools - select all pools that match the passed
// in object.
func SelectAllPools(p Pool) ([]Pool, error) {
	dbObj := []Pool{}

	p.Fencers = nil

	err := dbConn.Where(&p).Preload("Fencers").Find(&dbObj).Error
	if err != nil {
		log.Println(err)
	}

	for i := range dbObj {
		err = parsePool(&dbObj[i])
		if err != nil {
			log.Println(err)
		}
	}

	return dbObj, err
}
