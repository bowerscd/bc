package database

import (
	"errors"
	"log"
)

var errNoSuchRecord = errors.New("no such record")

// InsertFencer - insert a fencer into the database
func InsertFencer(f Fencer) error {
	err := dbConn.Create(&f).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// UpdateFencer - update a fencer in the database
// requires you to pass an old struct to identify the
// current record in the database.
//
//	The only updated fields will be:
// 		Name
//		Surname
//		PrimaryClubID
//		SecondaryClubID
func UpdateFencer(fNew, fOld Fencer) error {
	dbObj := Fencer{}

	err := dbConn.Where(&fOld).First(&dbObj).Error
	if err != nil {
		log.Println(err)
		return err
	}

	if dbObj.Name != fNew.Name {
		dbObj.Name = fNew.Name
	}

	if dbObj.Surname != fNew.Surname {
		dbObj.Surname = fNew.Surname
	}

	if dbObj.PrimaryClubID != fNew.PrimaryClubID {
		dbObj.PrimaryClubID = fNew.PrimaryClubID
	}

	if dbObj.SecondaryClubID != fNew.SecondaryClubID {
		dbObj.SecondaryClubID = fNew.SecondaryClubID
	}

	err = dbConn.Save(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// DeleteFencer - delete a fencer from the database
// throws an error if the delete did nothing
func DeleteFencer(f Fencer) error {
	db := dbConn.Where(&f).Delete(&f)

	err := db.Error
	affected := db.RowsAffected
	if err != nil {
		log.Println(err)
		return err
	}

	if affected == 0 {
		log.Println(errNoSuchRecord)
		return errNoSuchRecord
	}

	return nil
}

// SelectFencer - select a fencer from the database
// returns the first found record. There may be duplicates.
func SelectFencer(f Fencer) (Fencer, error) {
	dbObj := Fencer{}

	err := dbConn.Where(&f).First(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// SelectAllFencer - select all fencers that match the passed
// in object.
func SelectAllFencer(f Fencer) ([]Fencer, error) {
	dbObj := []Fencer{}

	err := dbConn.Where(&f).Find(&dbObj).Error

	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// InsertClub - insert a club into the database
func InsertClub(c Club) error {
	err := dbConn.Create(&c).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// UpdateClub - update a club in the database
// requires you to pass an old struct to identify the
// current record in the database.
//
//	The only updated fields will be:
// 		Name
//		Abbreviation
func UpdateClub(cNew, cOld Club) error {
	dbObj := Club{}

	err := dbConn.Where(&cOld).First(&dbObj).Error
	if err != nil {
		log.Println(err)
		return err
	}

	if dbObj.Name != cNew.Name {
		dbObj.Name = cNew.Name
	}

	if dbObj.Abbreviation != cNew.Abbreviation {
		dbObj.Abbreviation = cNew.Abbreviation
	}

	err = dbConn.Save(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return err
}

// DeleteClub - delete a club from the database
// throws an error if the delete did nothing
func DeleteClub(c Club) error {
	db := dbConn.Where(&c).Delete(&c)

	err := db.Error
	affected := db.RowsAffected
	if err != nil {
		log.Println(err)
		return err
	}

	if affected == 0 {
		log.Println(errNoSuchRecord)
		return errNoSuchRecord
	}

	return nil
}

// SelectClub - Select a club from the database.
// Abbreviations are enforced to be unique - it is suggested querying
// happens that way.
func SelectClub(c Club) (Club, error) {
	dbObj := Club{}

	err := dbConn.Where(&c).First(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// SelectAllClub - select all clubs that match the passed
// in object.
func SelectAllClub(c Club) ([]Club, error) {
	dbObj := []Club{}

	err := dbConn.Where(&c).Find(&dbObj).Error

	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}
