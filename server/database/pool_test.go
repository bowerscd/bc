package database

import (
	"testing"
)

var bobAnderson = Fencer{
	Name:    "Bob",
	Surname: "Anderson",
}

var jakePeralta = Fencer{
	Name:    "Jake",
	Surname: "Peralta",
}

var amySantiago = Fencer{
	Name:    "Amy",
	Surname: "Santiago",
}

var fList = [][]Fencer{
	{
		johnJacobson,
		helloWorld,
		janeJohnson,
		weiWang,
		yumiSato,
	},
	{
		johnJacobson,
		bobAnderson,
		jakePeralta,
		amySantiago,
	},
	{
		johnJacobson,
		bobAnderson,
		jakePeralta,
	},
}

func PoolSetup(t *testing.T) []Fencer {
	Setup(t)

	err := InsertFencer(johnJacobson)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(helloWorld)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(janeJohnson)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(weiWang)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(yumiSato)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(bobAnderson)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(jakePeralta)
	if err != nil {
		t.Fatal(err)
	}
	err = InsertFencer(amySantiago)
	if err != nil {
		t.Fatal(err)
	}

	fl, err := SelectAllFencer(Fencer{})
	if err != nil {
		t.Fatal(err)
	}

	return fl
}

func TestUpdatePool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	p := Pool{}
	p.ID = 1

	p, err = SelectPool(p)
	if err != nil {
		t.Error(err)
	}

	Scores := [][]uint{
		[]uint{0, 4, 3, 2, 5, 5, 1},
		[]uint{1, 0, 1, 1, 2, 2, 1},
		[]uint{2, 2, 0, 5, 3, 4, 1},
		[]uint{1, 5, 3, 0, 1, 2, 1},
		[]uint{3, 4, 1, 2, 0, 3, 1},
		[]uint{4, 5, 5, 5, 5, 0, 1},
		[]uint{5, 5, 3, 2, 5, 5, 0},
	}

	err = UpdatePool(p, Scores)
	if err != nil {
		t.Error(err)
	}

}

func TestUpdateBadFormatPool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	p := Pool{}
	p.ID = 1

	p, err = SelectPool(p)
	if err != nil {
		t.Error(err)
	}

	Scores := [][]uint{
		[]uint{0, 4, 3, 2, 5, 5, 1},
		[]uint{1, 5, 1, 1, 2, 2, 1},
		[]uint{2, 2, 0, 5, 3, 4, 1},
		[]uint{1, 5, 3, 0, 1, 2, 1},
		[]uint{3, 4, 1, 2, 0, 3, 1},
		[]uint{4, 5, 5, 5, 5, 0, 1},
		[]uint{5, 5, 3, 2, 5, 5, 0},
	}

	err = UpdatePool(p, Scores)
	if err != ErrValidationFailed {
		t.Error("Wrong Error Returned")
	}

	Scores = [][]uint{
		[]uint{0, 4, 3, 2, 5, 5, 1},
		[]uint{4, 0, 1, 1, 2, 2, 1},
		[]uint{2, 2, 0, 5, 3, 4, 1},
		[]uint{1, 5, 3, 0, 1, 2, 1},
		[]uint{3, 4, 1, 2, 0, 3, 1},
		[]uint{4, 5, 5, 5, 5, 0, 1},
		[]uint{5, 5, 3, 2, 5, 5, 0},
	}

	err = UpdatePool(p, Scores)
	if err != ErrValidationFailed {
		t.Error("Wrong Error Returned")
	}

}

func TestUpdateFinishedPool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	p := Pool{}
	p.ID = 1

	p, err = SelectPool(p)
	if err != nil {
		t.Error(err)
	}

	err = SetPoolPublishState(p.ID, true)
	if err != nil {
		t.Fatal(err)
	}

	pp := Pool{}
	pp.ID = p.ID

	p, err = SelectPool(pp)

	Scores := [][]uint{
		[]uint{0, 4, 3, 2, 5, 5, 1},
		[]uint{1, 0, 1, 1, 2, 2, 1},
		[]uint{2, 2, 0, 5, 3, 4, 1},
		[]uint{1, 5, 3, 0, 1, 2, 1},
		[]uint{3, 4, 1, 2, 0, 3, 1},
		[]uint{4, 5, 5, 5, 5, 0, 1},
		[]uint{5, 5, 3, 2, 5, 5, 0},
	}

	err = UpdatePool(p, Scores)
	if err != ErrPoolComplete {
		t.Error("Wrong Error Returned")
	}

}

func TestGeneratePool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}
}

func TestDeletePool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	p := Pool{}

	p.ID = 1

	err = DeletePool(p)
	if err != nil {
		t.Error(err)
	}
}

func TestSelectPool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	p := Pool{}

	p.ID = 1

	p, err = SelectPool(p)
	if err != nil {
		t.Error(err)
	}

	if len(p.VictoryPercentages) != len(fl) ||
		len(p.TouchesScored) != len(fl) ||
		len(p.TouchesRecieved) != len(fl) ||
		len(p.Scores) != len(fl) ||
		len(p.Scores[0]) != len(fl) ||
		len(p.Placement) != len(fl) ||
		len(p.Indicator) != len(fl) ||
		len(p.Fencers) != len(fl) {
		t.Errorf(`
		One of the following does not match:
			(V)    %d != %d
			(TS)   %d != %d
			(TR)   %d != %d
			(S)    %d != %d
			(S[0]) %d != %d
			(Pl)   %d != %d
			(I)    %d != %d
			(F)    %d != %d
		`,
			len(p.VictoryPercentages), len(fl),
			len(p.TouchesScored), len(fl),
			len(p.TouchesRecieved), len(fl),
			len(p.Scores), len(fl),
			len(p.Scores[0]), len(fl),
			len(p.Placement), len(fl),
			len(p.Indicator), len(fl),
			len(p.Fencers), len(fl))
	}
}

func TestPoolPublishState(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	err = SetPoolPublishState(1, true)
	if err != nil {
		t.Error(err)
	}

	err = SetPoolPublishState(1, false)
	if err != nil {
		t.Error(err)
	}
}

func TestSelectAllPool(t *testing.T) {
	fl := PoolSetup(t)
	defer Teardown(t)

	err := GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	err = GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	err = GeneratePool(fl)
	if err != nil {
		t.Error(err)
	}

	pl, err := SelectAllPools(Pool{})
	if err != nil {
		t.Error(err)
	}

	if len(pl) != 3 {
		t.Error("should have 3 pools")
	}

	for _, p := range pl {
		if len(p.VictoryPercentages) != len(fl) ||
			len(p.TouchesScored) != len(fl) ||
			len(p.TouchesRecieved) != len(fl) ||
			len(p.Scores) != len(fl) ||
			len(p.Scores[0]) != len(fl) ||
			len(p.Placement) != len(fl) ||
			len(p.Indicator) != len(fl) ||
			len(p.Fencers) != len(fl) {
			t.Errorf(`
		One of the following does not match:
			(V)    %d != %d
			(TS)   %d != %d
			(TR)   %d != %d
			(S)    %d != %d
			(S[0]) %d != %d
			(Pl)   %d != %d
			(I)    %d != %d
			(F)    %d != %d
		`,
				len(p.VictoryPercentages), len(fl),
				len(p.TouchesScored), len(fl),
				len(p.TouchesRecieved), len(fl),
				len(p.Scores), len(fl),
				len(p.Scores[0]), len(fl),
				len(p.Placement), len(fl),
				len(p.Indicator), len(fl),
				len(p.Fencers), len(fl))
		}
	}
}

func TestNoPool(t *testing.T) {
	_ = PoolSetup(t)
	defer Teardown(t)

	_, err := SelectPool(Pool{})
	if err == nil {
		t.Error(errShouldError)
	}

	err = UpdatePool(Pool{}, [][]uint{})
	if err == nil {
		t.Error(errShouldError)
	}

	err = DeletePool(Pool{})
	if err == nil {
		t.Error(errShouldError)
	}

	err = SetPoolPublishState(1, false)
	if err == nil {
		t.Error(errShouldError)
	}
}
