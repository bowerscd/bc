package database

import "github.com/jinzhu/gorm"

// Fencer - represents a fencer in the database model
type Fencer struct {
	gorm.Model
	Name            string `gorm:"not null"`
	Surname         string `gorm:"not null"`
	PrimaryClub     Club   `gorm:"foreignkey:PrimaryClubID" json:"-"`
	SecondaryClub   Club   `gorm:"foreignkey:SecondaryClubID" json:"-"`
	PrimaryClubID   uint
	SecondaryClubID uint
}

// Club - represents a club in the database model
type Club struct {
	gorm.Model
	Name         string `gorm:"not null"`
	Abbreviation string `gorm:"not null;UNIQUE"`
}

// Pool - represents a pool in the database model
type Pool struct {
	gorm.Model

	Fencers    []Fencer `gorm:"many2many:PoolFencer"`
	RawData    string   `gorm:"NOT NULL" json:"-"`
	IsComplete bool

	PoolInternal
}

// PoolInternal - the internal representation of the pool
type PoolInternal struct {
	Scores             [][]uint32 `gorm:"-"`
	VictoryPercentages []float32  `gorm:"-"`
	TouchesScored      []uint32   `gorm:"-"`
	TouchesRecieved    []uint32   `gorm:"-"`
	Indicator          []int8     `gorm:"-"`
	Placement          []uint32   `gorm:"-"`
}
