package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/bowerscd/bc/server/database"
)

var johnJacobson = database.Fencer{
	Name:    "John",
	Surname: "Jacobson",
}

var helloWorld = database.Fencer{
	Name:    "Hello",
	Surname: "World",
}

var weiWang = database.Fencer{
	Name:    "伟",
	Surname: "王",
}

var yumiSato = database.Fencer{
	Name:    "夕実",
	Surname: "佐藤",
}

const DBURI string = "./testdb.sqlite"

func setup(t *testing.T) *mux.Router {
	log.SetFlags(log.Llongfile)

	if _, err := os.Stat(DBURI); err == nil {
		os.Remove(DBURI)
	}

	r, err := ConfigureHandlers(DBURI)
	if err != nil {
		log.Fatal(err)
	}

	return r
}

func teardown(t *testing.T) {
	database.CloseDB()
}

func TestFencerAddHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Fencer{
		johnJacobson,
		helloWorld,
		yumiSato,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/fencer/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}
}

func TestFencerUpdateHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Fencer{
		johnJacobson,
		helloWorld,
		yumiSato,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/fencer/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	f := database.Fencer{
		Name:    "Jackson",
		Surname: "Kahn",
	}

	b, err := json.Marshal(f)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/fencer/%d", 1)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

}

func TestFencerBadURI(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	urls := map[string][]string{
		"/pool/%s/order":         []string{"GET"},
		"/pool/%s":               []string{"GET"},
		"/auth/pool/%s/update":   []string{"POST"},
		"/auth/pool/%s/finish":   []string{"POST"},
		"/auth/pool/%s/unfinish": []string{"POST"},
		"/auth/pool/%s":          []string{"DELETE"},
		"/club/%s":               []string{"GET"},
		"/auth/club/%s":          []string{"DELETE", "POST"},
		"/fencer/%s":             []string{"GET"},
		"/auth/fencer/%s":        []string{"POST", "DELETE"},
	}

	badFormats := []string{
		"asasdfa",
		"-1",
	}

	for urlFstr, methods := range urls {

		for _, method := range methods {

			for _, format := range badFormats {

				url := fmt.Sprintf(urlFstr, format)

				req, err := http.NewRequest(method, url, nil)
				if err != nil {
					t.Fatal(err)
				}

				rr := httptest.NewRecorder()

				router.ServeHTTP(rr, req)

				if status := rr.Code; status != http.StatusBadRequest {
					t.Errorf("handler returned wrong status code: got %v want %v",
						status, http.StatusBadRequest)
				}
			}
		}
	}
}

func TestFencerNoBody(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	url := fmt.Sprintf("/auth/fencer/%d", 1)

	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestFencerEmptyBody(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	url := fmt.Sprintf("/auth/fencer/%d", 1)

	req, err := http.NewRequest("POST", url, bytes.NewBufferString(""))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestFencerGetHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Fencer{
		johnJacobson,
		helloWorld,
		yumiSato,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/fencer/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Fatalf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	for index, tt := range data {

		url := fmt.Sprintf("/fencer/%d", index+1)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		b, err := ioutil.ReadAll(rr.Body)
		if err != nil {
			t.Fatal(err)
		}

		f := database.Fencer{}

		err = json.Unmarshal(b, &f)
		if err != nil {
			t.Fatal(err)
		}

		if tt.Surname != f.Surname ||
			tt.Name != f.Name ||
			uint(index+1) != f.ID {
			t.Fatalf("One of the following fields do not match: \n\t(surname) %s != %s\n\t(name) %s != %s\n\t(id) %d != %d\n",
				tt.Surname, f.Surname,
				tt.Name, f.Name,
				index+1, f.ID)
		}
	}
}

func TestFencerNoFencer(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	url := fmt.Sprintf("/fencer/%d", 1)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	url2 := fmt.Sprintf("/auth/fencer/%d", 1)

	req, err = http.NewRequest("DELETE", url2, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}
}

func TestFencerDeleteHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Fencer{
		johnJacobson,
		helloWorld,
		yumiSato,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/fencer/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	for index := range data {
		url := fmt.Sprintf("/auth/fencer/%d", index+1)

		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}
}
