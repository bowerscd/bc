package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/bowerscd/bc/server/database"
)

var bobAnderson = database.Fencer{
	Name:    "Bob",
	Surname: "Anderson",
}

var jakePeralta = database.Fencer{
	Name:    "Jake",
	Surname: "Peralta",
}

var amySantiago = database.Fencer{
	Name:    "Amy",
	Surname: "Santiago",
}

var janeJohnson = database.Fencer{
	Name:    "Jane",
	Surname: "Johnson",
}

func PoolSetup(t *testing.T) (*mux.Router, []database.Fencer) {
	router := setup(t)

	err := database.InsertFencer(johnJacobson)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(helloWorld)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(janeJohnson)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(weiWang)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(yumiSato)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(bobAnderson)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(jakePeralta)
	if err != nil {
		t.Fatal(err)
	}
	err = database.InsertFencer(amySantiago)
	if err != nil {
		t.Fatal(err)
	}

	fl, err := database.SelectAllFencer(database.Fencer{})
	if err != nil {
		t.Fatal(err)
	}

	return router, fl
}

func TestGeneratePool(t *testing.T) {
	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

}

func TestUpdatePool(t *testing.T) {
	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	getURL := fmt.Sprintf("/pool/%d", 1)

	req, err = http.NewRequest("GET", getURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	b, err = ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}

	pool := database.Pool{}

	err = json.Unmarshal(b, &pool)
	if err != nil {
		t.Error(err)
	}

	Scores := [][]uint{
		[]uint{0, 4, 3, 2, 5, 5, 1},
		[]uint{1, 0, 1, 1, 2, 2, 1},
		[]uint{2, 2, 0, 5, 3, 4, 1},
		[]uint{1, 5, 3, 0, 1, 2, 1},
		[]uint{3, 4, 1, 2, 0, 3, 1},
		[]uint{4, 5, 5, 5, 5, 0, 1},
		[]uint{5, 5, 3, 2, 5, 5, 0},
	}

	b, err = json.Marshal(Scores)

	updateURL := fmt.Sprintf("/auth/pool/%d/update", 1)

	req, err = http.NewRequest("POST", updateURL, bytes.NewBuffer(b))
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}
}

func TestDeletePool(t *testing.T) {
	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	delURL := fmt.Sprintf("/auth/pool/%d", 1)

	req, err = http.NewRequest("DELETE", delURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}
}

func TestFinishUnfinishPool(t *testing.T) {
	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	finishURL := fmt.Sprintf("/auth/pool/%d/finish", 1)
	unfinishURL := fmt.Sprintf("/auth/pool/%d/unfinish", 1)

	req, err = http.NewRequest("POST", finishURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	req, err = http.NewRequest("POST", unfinishURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}
}

func TestNoPool(t *testing.T) {
	r, _ := PoolSetup(t)
	defer teardown(t)

	getURL := fmt.Sprintf("/pool/%d", 1)
	orderURL := fmt.Sprintf("/pool/%d/order", 1)
	updateURL := fmt.Sprintf("/auth/pool/%d/update", 1)
	delURL := fmt.Sprintf("/auth/pool/%d", 1)

	p := [][]uint32{}

	b, err := json.Marshal(&p)
	if err != nil {
		t.Fatal(err)
	}

	delReq, err := http.NewRequest("DELETE", delURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	getReq, err := http.NewRequest("GET", getURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	updateReq, err := http.NewRequest("POST", updateURL, bytes.NewBuffer(b))
	if err != nil {
		t.Fatal(err)
	}

	orderReq, err := http.NewRequest("GET", orderURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, delReq)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, updateReq)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, getReq)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, orderReq)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}
}

func TestMarshallErrors(t *testing.T) {
	r, _ := PoolSetup(t)
	defer teardown(t)

	badJSON := bytes.NewBufferString("{}")

	const genURL = "/auth/pool/generate"
	const method = "POST"
	const updateURL = "/auth/pool/1/update"

	genReq, err := http.NewRequest(method, genURL, badJSON)
	genReq.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, genReq)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	updateReq, err := http.NewRequest(method, updateURL, badJSON)
	updateReq.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, updateReq)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestOrder(t *testing.T) {
	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	orderURL := fmt.Sprintf("/pool/%d/order", 1)

	req, err = http.NewRequest("GET", orderURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	b, err = ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}

	order := [][]database.Fencer{}

	err = json.Unmarshal(b, &order)
	if err != nil {
		t.Fatal(err)
	}

	expected := [][]database.Fencer{
		[]database.Fencer{fl[1], fl[2]},
		[]database.Fencer{fl[0], fl[4]},
		[]database.Fencer{fl[6], fl[3]},
		[]database.Fencer{fl[5], fl[7]},
		[]database.Fencer{fl[0], fl[1]},
		[]database.Fencer{fl[2], fl[3]},
		[]database.Fencer{fl[4], fl[5]},
		[]database.Fencer{fl[7], fl[6]},
		[]database.Fencer{fl[3], fl[0]},
		[]database.Fencer{fl[4], fl[1]},
		[]database.Fencer{fl[7], fl[2]},
		[]database.Fencer{fl[5], fl[6]},
		[]database.Fencer{fl[3], fl[1]},
		[]database.Fencer{fl[7], fl[0]},
		[]database.Fencer{fl[6], fl[4]},
		[]database.Fencer{fl[2], fl[5]},
		[]database.Fencer{fl[1], fl[7]},
		[]database.Fencer{fl[6], fl[3]},
		[]database.Fencer{fl[5], fl[0]},
		[]database.Fencer{fl[2], fl[6]},
		[]database.Fencer{fl[3], fl[7]},
		[]database.Fencer{fl[1], fl[5]},
		[]database.Fencer{fl[2], fl[4]},
		[]database.Fencer{fl[0], fl[6]},
		[]database.Fencer{fl[3], fl[5]},
		[]database.Fencer{fl[7], fl[4]},
		[]database.Fencer{fl[6], fl[1]},
		[]database.Fencer{fl[0], fl[2]},
	}

	for i := range expected {
		if expected[i][0].ID != order[i][0].ID ||
			expected[i][1].ID != order[i][1].ID ||
			expected[i][0].Name != order[i][0].Name ||
			expected[i][1].Name != order[i][1].Name ||
			expected[i][0].Surname != order[i][0].Surname ||
			expected[i][1].Surname != order[i][1].Surname {
			t.Errorf(`
			[INDEX: %d] Values Should Be Equal:
				EXPECTED: ([%d] %s %s, [%d] %s %s) vs GOT: ([%d] %s %s, [%d] %s %s)`,
				i,
				expected[i][0].ID,
				expected[i][0].Name, expected[i][0].Surname,
				expected[i][1].ID,
				expected[i][1].Name, expected[i][1].Surname,
				order[i][0].ID,
				order[i][0].Name, order[i][0].Surname,
				order[i][0].ID,
				order[i][1].Name, order[i][1].Surname)
		}
	}

}

func TestSelectPool(t *testing.T) {

	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	getURL := fmt.Sprintf("/pool/%d", 1)

	req, err = http.NewRequest("GET", getURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	b, err = ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}

	pool := database.Pool{}

	err = json.Unmarshal(b, &pool)
	if err != nil {
		t.Error(err)
	}

	if len(pool.VictoryPercentages) != len(fl) ||
		len(pool.TouchesScored) != len(fl) ||
		len(pool.TouchesRecieved) != len(fl) ||
		len(pool.Scores) != len(fl) ||
		len(pool.Scores[0]) != len(fl) ||
		len(pool.Placement) != len(fl) ||
		len(pool.Indicator) != len(fl) ||
		len(pool.Fencers) != len(fl) {
		t.Errorf(`
			One of the following does not match:
				(V)    %d != %d
				(TS)   %d != %d
				(TR)   %d != %d
				(S)    %d != %d
				(S[0]) %d != %d
				(Pl)   %d != %d
				(I)    %d != %d
				(F)    %d != %d
			`,
			len(pool.VictoryPercentages), len(fl),
			len(pool.TouchesScored), len(fl),
			len(pool.TouchesRecieved), len(fl),
			len(pool.Scores), len(fl),
			len(pool.Scores[0]), len(fl),
			len(pool.Placement), len(fl),
			len(pool.Indicator), len(fl),
			len(pool.Fencers), len(fl))
	}
}

func TestPoolNoBody(t *testing.T) {

	r, fl := PoolSetup(t)
	defer teardown(t)

	b, err := json.Marshal(fl)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/pool/generate")

	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	req, err = http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

	getURL := fmt.Sprintf("/pool/%d", 1)

	req, err = http.NewRequest("GET", getURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	b, err = ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}

	updateURL := fmt.Sprintf("/auth/pool/%d/update", 1)

	req, err = http.NewRequest("POST", updateURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

}
