package handlers

import (
	"github.com/gorilla/mux"
	"gitlab.com/bowerscd/bc/server/database"
)

// ConfigureHandlers - function to globally configure all the HTTP handlers in this program
func ConfigureHandlers(dbURI string) (*mux.Router, error) {
	r := mux.NewRouter()

	err := database.ConnectToDB(database.SQLITE, dbURI)
	if err != nil {
		return nil, err
	}

	r.HandleFunc("/pool/{id}", getPoolHandler).
		Methods("GET")
	r.HandleFunc("/pool/{id}/order", poolOrderHandler).
		Methods("GET")
	r.HandleFunc("/auth/pool/generate", generatePoolHandler).
		Methods("POST")
	r.HandleFunc("/auth/pool/{id}/update", postPoolHandler).
		Methods("POST")
	r.HandleFunc("/auth/pool/{id}/finish", finishPoolHandler).
		Methods("POST")
	r.HandleFunc("/auth/pool/{id}/unfinish", unfinishPoolHandler).
		Methods("POST")
	r.HandleFunc("/auth/pool/{id}", deletePoolHandler).
		Methods("DELETE")

	r.HandleFunc("/club/{id}", getClubHandler).
		Methods("GET")
	r.HandleFunc("/auth/club/{id}", postClubHandler).
		Methods("POST")
	r.HandleFunc("/auth/club/{id}", deleteClubHandler).
		Methods("DELETE")

	r.HandleFunc("/fencer/{id}", getFencerHandler).
		Methods("GET")
	r.HandleFunc("/auth/fencer/{id}", postFencerHandler).
		Methods("POST")
	r.HandleFunc("/auth/fencer/{id}", deleteFencerHandler).
		Methods("DELETE")

	return r, nil
}
