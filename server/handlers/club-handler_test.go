package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/bowerscd/bc/server/database"
)

var bostonFC = database.Club{
	Name:         "Boston Fencing Club",
	Abbreviation: "BFC",
}

var progFC = database.Club{
	Name:         "Programming Club Unlimited",
	Abbreviation: "PCU",
}

var sapporoClub = database.Club{
	Name:         "札幌フェンシング協会練習会",
	Abbreviation: "SC",
}

func TestClubAddHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/club/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}
}

func TestClubUpdateHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/club/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	f := database.Club{
		Name:         "Kahn Happy Narcoleptics",
		Abbreviation: "KHN",
	}

	b, err := json.Marshal(f)
	if err != nil {
		t.Fatal(err)
	}

	url := fmt.Sprintf("/auth/club/%d", 1)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNoContent)
	}

}

func TestClubBadURI(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	for _, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/club/%s", "asd")

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}

		url2 := fmt.Sprintf("/club/%s", "asd")

		req2, err := http.NewRequest("GET", url2, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr = httptest.NewRecorder()

		router.ServeHTTP(rr, req2)
		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}

		req3, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr = httptest.NewRecorder()

		router.ServeHTTP(rr, req3)
		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusBadRequest)
		}
	}
}

func TestClubNoBody(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	url := fmt.Sprintf("/auth/club/%d", 1)

	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestClubEmptyBody(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	url := fmt.Sprintf("/auth/club/%d", 1)

	req, err := http.NewRequest("POST", url, bytes.NewBufferString(""))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestClubGetHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/club/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Fatalf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	for index, tt := range data {

		url := fmt.Sprintf("/club/%d", index+1)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		b, err := ioutil.ReadAll(rr.Body)
		if err != nil {
			t.Fatal(err)
		}

		f := database.Club{}

		err = json.Unmarshal(b, &f)
		if err != nil {
			t.Fatal(err)
		}

		if tt.Abbreviation != f.Abbreviation ||
			tt.Name != f.Name ||
			uint(index+1) != f.ID {
			t.Fatalf("One of the following fields do not match: \n\t(Abbreviation) %s != %s\n\t(name) %s != %s\n\t(id) %d != %d\n",
				tt.Abbreviation, f.Abbreviation,
				tt.Name, f.Name,
				index+1, f.ID)
		}
	}
}

func TestClubNoClub(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	url := fmt.Sprintf("/club/%d", 1)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	url2 := fmt.Sprintf("/auth/club/%d", 1)

	req, err = http.NewRequest("DELETE", url2, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}
}

func TestClubDeleteHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	data := []database.Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	for index, tt := range data {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/club/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	for index := range data {
		url := fmt.Sprintf("/auth/club/%d", index+1)

		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}
}
