package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/bowerscd/bc/server/database"
)

func TestCombinedAddHandler(t *testing.T) {
	router := setup(t)
	defer teardown(t)

	clubData := []database.Club{
		bostonFC,
		progFC,
		sapporoClub,
	}

	fencerData := []database.Fencer{
		johnJacobson,
		helloWorld,
		yumiSato,
		weiWang,
	}

	for index, tt := range clubData {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/club/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	clubDatabases := make([]database.Club, 3)

	for i, tt := range clubData {
		url := fmt.Sprintf("/club/%d", i+1)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		b, err := ioutil.ReadAll(rr.Body)
		if err != nil {
			t.Fatal(err)
		}

		err = json.Unmarshal(b, &clubDatabases[i])
		if err != nil {
			t.Fatal(err)
		}

		if tt.Abbreviation != clubDatabases[i].Abbreviation ||
			tt.Name != clubDatabases[i].Name ||
			uint(i+1) != clubDatabases[i].ID {
			t.Fatalf("One of the following fields do not match: \n\t(abbreviation) %s != %s\n\t(name) %s != %s\n\t(id) %d != %d\n",
				tt.Abbreviation, clubDatabases[i].Abbreviation,
				tt.Name, clubDatabases[i].Name,
				i+1, clubDatabases[i].ID)
		}
	}

	for index, tt := range fencerData {

		b, err := json.Marshal(tt)
		if err != nil {
			t.Fatal(err)
		}

		url := fmt.Sprintf("/auth/fencer/%d", index+1)

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}

		url2 := fmt.Sprintf("/fencer/%d", index+1)
		req2, err := http.NewRequest("GET", url2, bytes.NewBuffer(b))
		if err != nil {
			t.Fatal(err)
		}

		router.ServeHTTP(rr, req2)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}

		b, err = ioutil.ReadAll(rr.Body)
		if err != nil {
			t.Fatal(err)
		}

		f := database.Fencer{}

		err = json.Unmarshal(b, &f)
		if err != nil {
			t.Fatal(err)
		}

		i := index

		if index > len(clubDatabases)-1 {
			i = len(clubDatabases) - 1
		}

		f.PrimaryClub = clubDatabases[i]
		f.PrimaryClubID = clubDatabases[i].ID

		bPrime, err := json.Marshal(f)
		if err != nil {
			t.Fatal(err)
		}

		req3, err := http.NewRequest("POST", url, bytes.NewBuffer(bPrime))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}

		router.ServeHTTP(rr, req3)

		if status := rr.Code; status != http.StatusNoContent {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusNoContent)
		}
	}

	for index, tt := range fencerData {

		url := fmt.Sprintf("/fencer/%d", index+1)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		b, err := ioutil.ReadAll(rr.Body)
		if err != nil {
			t.Fatal(err)
		}

		f := database.Fencer{}

		err = json.Unmarshal(b, &f)
		if err != nil {
			t.Fatal(err)
		}

		i := index + 1

		if index > len(clubDatabases)-1 {
			i = len(clubDatabases)
		}

		if tt.Surname != f.Surname ||
			tt.Name != f.Name ||
			uint(index+1) != f.ID ||
			uint(i) != f.PrimaryClubID {
			t.Fatalf("One of the following fields do not match: \n\t(surname) %s != %s\n\t(name) %s != %s\n\t(id) %d != %d\n\t(primaryclub) %d != %d\n",
				tt.Surname, f.Surname,
				tt.Name, f.Name,
				index+1, f.ID,
				i+1, f.PrimaryClubID)
		}
	}
}
