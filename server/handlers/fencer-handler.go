package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/bowerscd/bc/server/database"

	"github.com/gorilla/mux"
)

func getFencerHandler(w http.ResponseWriter, r *http.Request) {
	fencerid, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	f := database.Fencer{}

	f.ID = uint(fencerid)

	f, err = database.SelectFencer(f)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	b, err := json.Marshal(f)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(b)
}

func postFencerHandler(w http.ResponseWriter, r *http.Request) {

	fencerid, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	f := database.Fencer{}

	f.ID = uint(fencerid)

	reader := r.Body
	if reader == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	b, err := ioutil.ReadAll(reader)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}

	fNew := database.Fencer{}

	err = json.Unmarshal(b, &fNew)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	f, err = database.SelectFencer(f)
	if err != nil {
		err = database.InsertFencer(fNew)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusNoContent)
		return
	}

	err = database.UpdateFencer(fNew, f)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func deleteFencerHandler(w http.ResponseWriter, r *http.Request) {
	fencerid, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	f := database.Fencer{}

	f.ID = uint(fencerid)

	f, err = database.SelectFencer(f)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = database.DeleteFencer(f)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
