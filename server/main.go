package main

import (
	"log"
	"net/http"

	"gitlab.com/bowerscd/bc/server/database"
	"gitlab.com/bowerscd/bc/server/handlers"
)

func populateTestDb() {

	johnJacobson := database.Fencer{
		Name:    "John",
		Surname: "Jacobson",
	}

	bostonFC := database.Club{
		Name:         "Boston Fencing Club",
		Abbreviation: "BFC",
	}

	err := database.InsertFencer(johnJacobson)
	if err != nil {
		log.Fatalln(err)
	}

	c, err := database.SelectClub(bostonFC)
	if err != nil {
		log.Fatalln(err)
	}

	f, err := database.SelectFencer(johnJacobson)
	if err != nil {
		log.Fatalln(err)
	}

	newF := f

	newF.PrimaryClub = c

	err = database.UpdateFencer(newF, f)
	if err != nil {
		log.Fatalln(err)
	}

}

func main() {
	r, err := handlers.ConfigureHandlers("./testdb.sqlite")
	if err != nil {
		return
	}

	populateTestDb()

	http.Handle("/", r)
	http.ListenAndServe(":3030", r)
}
